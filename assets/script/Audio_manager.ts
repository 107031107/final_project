const {ccclass, property} = cc._decorator;

@ccclass
export default class Audio extends cc.Component {

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    public bgmState = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.bgmState = cc.audioEngine.playMusic(this.bgm, true);       
    }

    // update (dt) {}
}
