// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Store_item_script extends cc.Component {

    private price : number = null;
    private total_manager:any = null;
    private store_manager:any = null;
    private item_cover:any = null;
    issold : boolean = false;
    ischoosed : boolean = false;
    private skin_number: number = 0;


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        this.store_manager = cc.find("Store_manager").getComponent("Store_manager");
        this.item_cover = this.node.parent.getChildByName("item cover");
    }

    start () {
        
        this.price = +this.node.parent.getChildByName("item frame").getChildByName("price").getComponent(cc.Label).string;
        if(this.issold){
            this.node.parent.getChildByName("item frame").getChildByName("price").active = false;
            this.node.parent.getChildByName("item frame").getChildByName("Sold").active = true;
        }
        if(this.ischoosed){
            this.change_to_choosed_sprite();
        }
        console.log("price: " + this.price);
    }

    // update (dt) {}

    choose(event, customEventData){
        console.log("fruit_cur: " + this.total_manager.fruit);
        var skin_number = this.skin_number;
        if(!this.issold){
            if(this.total_manager.fruit >= this.price){
                //change price to sold
                this.node.parent.getChildByName("item frame").getChildByName("price").active = false;
                this.node.parent.getChildByName("item frame").getChildByName("Sold").active = true;
                
                // update fruit
                this.total_manager.fruit -= this.price;
                this.total_manager.update_to_firebase("fruit", this.total_manager.fruit);
                this.store_manager.player_fruit_label.string = "fruits : " + this.total_manager.fruit;

                this.update_skin();
                
                this.update_current_skin();
                
                console.log("fruit_cft: " + this.total_manager.fruit);
                console.log("price: " + this.price);
                this.ischoosed = true;
                this.issold = true;
                
                alert("You bought a new skin!");
            }
            else{
                alert("You don't have enough fruit!");
            }
        }
        else{
            if(!this.ischoosed){
                this.update_current_skin();
                this.ischoosed = true;
                alert("change skin success!");
            }
        }
    }

    update_skin(){
        this.total_manager.skin += Math.pow(2,(this.skin_number-1));
        this.total_manager.update_to_firebase("skin", this.total_manager.skin)
    }

    update_current_skin(){

        var previous_skin = this.store_manager.items[this.total_manager.current_skin-1].getChildByName("Store_item_script").getComponent("Store_item_script");
        previous_skin.ischoosed = false;
        previous_skin.change_to_unchoosed_sprite();

        this.total_manager.current_skin = this.skin_number;
        this.total_manager.update_to_firebase("current_skin", this.skin_number);
        this.change_to_choosed_sprite();
    }

    change_to_choosed_sprite(){
        console.log("change skin " + this.skin_number +" to choosed sprite!")
        //TODO : change to choosed sprite, maybe be darker 
        this.item_cover.opacity = 70;


    }

    change_to_unchoosed_sprite(){
        console.log("change skin " + this.skin_number +" to unchoosed sprite!")
        //TODO : change to unchoosed sprite, maybe be lighter 
        this.item_cover.opacity = 0;

    }


    // use binary code to determine whether bought it or not 
    isbought(skin_number : number){

        var tmp_skin_number = this.total_manager.skin;
        for(let i = 1 ; i < skin_number ; i++){
            tmp_skin_number =  Math.floor(tmp_skin_number/2);
        }
        console.log("tmp_skin_number : "+ tmp_skin_number);
        if(tmp_skin_number%2){
            return true;
        }
        else{
            return false;
        }
    }   


    set_skin_number(skin_number:number){
        this.skin_number = skin_number;
    }

}
