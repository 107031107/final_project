"use strict";
cc._RF.push(module, 'cf5f9nYSlhHnKavwPPu3/Vb', 'Game_manager');
// script/Game_manager.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Game_manager = /** @class */ (function (_super) {
    __extends(Game_manager, _super);
    function Game_manager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pause_box = null;
        _this.buttons_parent = null;
        _this.player = null;
        _this.fruits = [];
        _this.total_manager = null;
        _this.stage_fruit = 0;
        _this.UI_frame = null;
        _this.UI_fruit_label = null;
        _this.UI_cell_label = null;
        _this.is_pausing = false;
        return _this;
        // update (dt) {}
    }
    Game_manager.prototype.onLoad = function () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
    };
    Game_manager.prototype.start = function () {
        if (this.total_manager.stage == 1)
            this.stage_fruit = this.total_manager.stage1_fruit;
        else if (this.total_manager.stage == 2)
            this.stage_fruit = this.total_manager.stage2_fruit;
        else if (this.total_manager.stage == 3)
            this.stage_fruit = this.total_manager.stage3_fruit;
        this.UI_frame = cc.find("Canvas/UI frame");
        this.UI_cell_label = this.UI_frame.getChildByName("cell label").getComponent(cc.Label);
        this.UI_fruit_label = this.UI_frame.getChildByName("fruit label").getComponent(cc.Label);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.init_game();
    };
    Game_manager.prototype.init_game = function () {
        for (var i = 0; i < 32; i++) {
            this.fruits[i].getComponent("Fruit").number = i + 1;
            if (this.stage_fruit % 2) {
                this.fruits[i].active = false;
            }
            this.stage_fruit = Math.floor(this.stage_fruit / 2);
            console.log("this.stage_fruit : " + this.stage_fruit);
        }
    };
    Game_manager.prototype.onKeyDown = function (event) {
        if (event.keyCode != cc.macro.KEY.escape)
            return;
        if (!this.is_pausing) {
            this.pause_box.setPosition(this.player.position);
            //this.pause_box.position = this.pause_box.position.sub(this.canvas.position);
            this.pause_box.active = true;
            cc.director.pause();
            this.pause_box.rotation = -cc.find("Canvas").getChildByName("Main Camera").getComponent("Camera_control").rotate_angle;
            this.buttons_parent.rotation = 2 * cc.find("Canvas").getChildByName("Main Camera").getComponent("Camera_control").rotate_angle;
            this.is_pausing = true;
        }
        else {
            this.cancel_pause();
        }
    };
    Game_manager.prototype.onKeyUp = function (event) {
    };
    Game_manager.prototype.cancel_pause = function () {
        this.pause_box.active = false;
        cc.director.resume();
        this.is_pausing = false;
    };
    Game_manager.prototype.back_to_menu = function () {
        cc.director.resume();
        this.total_manager.goto_menu();
    };
    Game_manager.prototype.go_to_win = function () {
        this.total_manager.goto_win_scene();
    };
    Game_manager.prototype.go_to_lose = function () {
        this.total_manager.goto_lose_scene();
    };
    /* UI stuff  */
    // camera_zoom_out_rate should be "this.camera_zoom_out_rate" in player , and UI will expand with camera
    // if camera is shrinking , then use "1/this.camera_zoom_out_rate" with camera instead
    Game_manager.prototype.adjust_UI_apprearance = function (camera_zoom_out_rate) {
        this.UI_frame.setPosition(this.UI_frame.x * 1 / camera_zoom_out_rate, this.UI_frame.y * 1 / camera_zoom_out_rate);
        this.UI_frame.scaleX *= 1 / camera_zoom_out_rate;
        this.UI_frame.scaleY *= 1 / camera_zoom_out_rate;
    };
    Game_manager.prototype.update_UI_fruit_lable = function (cur_fruit) {
        this.UI_fruit_label.string = "x " + String(cur_fruit);
    };
    Game_manager.prototype.update_UI_cell_lable = function (cur_cell) {
        this.UI_cell_label.string = "x " + String(cur_cell);
    };
    Game_manager.prototype.reset_fruit = function () {
        if (this.total_manager.stage == 1)
            this.total_manager.update_to_firebase("stage1_fruit", 0);
        else if (this.total_manager.stage == 2)
            this.total_manager.update_to_firebase("stage2_fruit", 0);
        else if (this.total_manager.stage == 3)
            this.total_manager.update_to_firebase("stage2_fruit", 0);
        alert("seccess! plaz logout and login");
    };
    __decorate([
        property({ type: cc.Node })
    ], Game_manager.prototype, "pause_box", void 0);
    __decorate([
        property({ type: cc.Node })
    ], Game_manager.prototype, "buttons_parent", void 0);
    __decorate([
        property({ type: cc.Node })
    ], Game_manager.prototype, "player", void 0);
    __decorate([
        property({ type: cc.Node })
    ], Game_manager.prototype, "fruits", void 0);
    Game_manager = __decorate([
        ccclass
    ], Game_manager);
    return Game_manager;
}(cc.Component));
exports.default = Game_manager;

cc._RF.pop();