"use strict";
cc._RF.push(module, '3a44dnffqtMxa1VA7rd0ueF', 'Audio_manager');
// script/Audio_manager.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Audio = /** @class */ (function (_super) {
    __extends(Audio, _super);
    function Audio() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bgm = null;
        _this.bgmState = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Audio.prototype.start = function () {
        this.bgmState = cc.audioEngine.playMusic(this.bgm, true);
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], Audio.prototype, "bgm", void 0);
    Audio = __decorate([
        ccclass
    ], Audio);
    return Audio;
}(cc.Component));
exports.default = Audio;

cc._RF.pop();