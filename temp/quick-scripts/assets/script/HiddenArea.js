(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/HiddenArea.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '157778YdNlPB7eiHY6irzrl', 'HiddenArea', __filename);
// script/HiddenArea.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var HiddenArea = /** @class */ (function (_super) {
    __extends(HiddenArea, _super);
    function HiddenArea() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HiddenArea.prototype.onload = function () {
        cc.director.getPhysicsManager().enabled = true;
    };
    HiddenArea.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.name == "player") {
            var action = cc.fadeOut(1);
            this.node.runAction(action);
            this.scheduleOnce(function () {
                this.node.destroy();
            }, 1);
        }
    };
    HiddenArea = __decorate([
        ccclass
    ], HiddenArea);
    return HiddenArea;
}(cc.Component));
exports.default = HiddenArea;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=HiddenArea.js.map
        