(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/Audio_manager.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '3a44dnffqtMxa1VA7rd0ueF', 'Audio_manager', __filename);
// script/Audio_manager.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Audio = /** @class */ (function (_super) {
    __extends(Audio, _super);
    function Audio() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.bgm = null;
        _this.bgmState = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    Audio.prototype.start = function () {
        this.bgmState = cc.audioEngine.playMusic(this.bgm, true);
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], Audio.prototype, "bgm", void 0);
    Audio = __decorate([
        ccclass
    ], Audio);
    return Audio;
}(cc.Component));
exports.default = Audio;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Audio_manager.js.map
        