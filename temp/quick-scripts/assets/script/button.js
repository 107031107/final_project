(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/button.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '8016aXrm3RMgLf1WlJSsQ0w', 'button', __filename);
// script/button.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var button = /** @class */ (function (_super) {
    __extends(button, _super);
    function button() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.active = true;
        return _this;
        // update (dt) {}
    }
    // onLoad () {}
    button.prototype.start = function () {
        this.node.on('mouseenter', function (event) {
            document.body.style.cursor = "pointer";
        });
        this.node.on('mouseleave', function (event) {
            document.body.style.cursor = "auto";
        });
    };
    button = __decorate([
        ccclass
    ], button);
    return button;
}(cc.Component));
exports.default = button;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=button.js.map
        